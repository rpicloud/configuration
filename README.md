# Ansible Playbook
This playbook is used to setup the whole docker swarm cluster.

## Inventory
Before starting the setup of the cluster the inventory file has to be configured
properly:

**all:**
This includes all hostnames used in the cluster

**swarm-servers:**
This group contains a list of all Raspberry Pis which are part of the cluster.

**admin-servers:**
This list includes only administration nodes. Administration nodes are used to
visualize the monitoring.

**manager-servers:**
This list includes all docker swarm manager nodes.

**worker-servers:**
This group contains all worker nodes.

## Roles

### 1. init
The init roles contains general configuration information to setup the Raspberry Pis

Execution:
```
ansible-playbook -k init.yml
```

### 2. NFS
The nfs role connects all Raspberry Pis to the NFS server hosted by the OpenWRT router.

Execution:
```
ansible-playbook -k nfs.yml
```

### 3. Kiosk mode
The kiosk-mode role sets up the admin servers for having a chrome browser in kiosk mode to show the monitoring dashboard.

Execution:
```
ansible-playbook -k kiosk-mode.yml
```

### 4. docker-swarm
This role sets up the docker swarm on swarm hosts.

Execution:
```
ansible-playbook -k docker-swarm.yml
```

### 5. Traefik
This role sets up the overlay network and the traefik load balancer

Execution:
```
ansible-playbook -k traefik.yml
```

### 6. Gogs
This role sets up the gogs

Execution:
```
ansible-playbook -k gogs.yml
```
